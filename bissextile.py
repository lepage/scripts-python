#test année bissextile
#une année est bissextile si
#elle est divisible par 4
#sauf si elle est divisible par 100
#sauf celles divisibles par 400 qui sont bissextiles
#exemples:
#2021: non
#2020: oui
#1900: non
#2000: oui
#oui: 1996, 1600, 2024
#non: 1800, 1999, 2100

annee=input("Entre une année: ")
if not annee.isdigit():
    print("Année non reconnue")
    exit()
    
annee=int(annee)
if annee%400==0:
    print("bissextile")
elif annee%100==0:
    print("non bissextile")
elif annee%4==0:
    print("bissextile")
else:
    print("non bissextile")
