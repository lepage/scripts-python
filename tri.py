#fonction qui prend en paramètre
#une liste
#et qui renvoie:
#l'indice et la valeur du minimum
#interdit d'utiliser min, max et sort
def findmin(liste):
    indice=0
    minimum=liste[0]
    for i in range(len(liste)):
        if liste[i]<minimum:
            minimum=liste[i]
            indice=i
    return indice,minimum

#échange les éléments d'indices i et j
#de la liste
def swap(liste,i,j):
    aux=liste[i]
    liste[i]=liste[j]
    liste[j]=aux

def tri_selection(liste):
    for j in range(len(liste)):
        i,v=findmin(liste[j:])
        swap(liste,i+j,j)
        #print(liste)

if __name__ == "__main__":
    liste=[8,4,0,6,1,5,7,2]
    print(liste)
    tri_selection(liste)
    print(liste)


#[8,4,0,6,1,5,7,2]
#                   4
#liste     [0,4,8,6,1,5,7,2]
#liste[1:]   [4,8,6,1,5,7,2]
#                   3

#[0,1,8,6,4,5,7,2]
#[0,1,2,6,4,5,7,8]
#[0,1,2,4,6,5,7,8]
#[0,1,2,4,5,6,7,8]
#[0,1,2,4,5,6,7,8]

