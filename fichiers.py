with open("blabla.txt","w") as fichier:
    fichier.write("blabla\n")
    fichier.write("blibli\n")
    fichier.write("blublu\n")


with open("blabla.txt","r") as fichier:
    texte=fichier.read()
    print("1ere lecture:\n"+texte)
    texte=fichier.read()
    print("2e lecture:\n"+texte)
    fichier.seek(0)
    texte=fichier.read()
    print("3e lecture:\n"+texte)
    

with open("blabla.txt","r") as fichier:
    texte=fichier.readlines()
    print(texte)
    