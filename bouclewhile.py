#on compte de 0 à 9

i=0
while i<10:
    print(i)
    i+=1
print("Fini")

#on calcule la moyenne des nombres
#entrés par l'utilisateur
#on s'arrête au mot "stop"

somme = 0
entree = ""
compteur=0
while entree!="stop":
    if entree.isdigit():
        somme += int(entree)
        compteur+=1
    entree = input("Entre un nombre ")

if compteur>0:
    print("moyenne",somme/compteur)
