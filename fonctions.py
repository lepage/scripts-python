#renvoie la valeur absolue d'un nombre
def mafonction(a):
    if a>0:
        return a
    else:
        return -a
    
print(mafonction(8))
print(mafonction(-6))

#teste si a>b
def fonction2(a,b):
    return a>b

print(fonction2(5,3))

#renvoie le premier élément d'une liste
def fonction3(liste):
    return liste[0]

#fonction qui prend en paramètre une liste
#de nombres
#et qui renvoie son plus grand élément
#interdit d'utiliser min(), max() et sort()

def maximum(liste):
    if len(liste)==0:
        return None
    resultat=liste[0]
    for e in liste:
        #si je trouve un élément
        #plus grand que le max actuel
        if e > resultat:
            #il devient mon nouveau max
            resultat=e
    return resultat

#test:
print(maximum([8,3,9,5]))
print(maximum([-4,-1,-1,-3]))
print(maximum([]))
print(maximum([100]))

#échange les éléments d'indices i et j
#de la liste
#par ex:
#swap([8,5,4,9,7],1,3) renvoie rien
#mais modifie la liste
#qui devient [8,9,4,5,7]
def swap(liste,i,j):
    aux=liste[i]
    liste[i]=liste[j]
    liste[j]=aux

liste=[8,1,7,3,6,8,9]
print(liste)
swap(liste,2,3)
print(liste)

#fonction qui teste si une liste
#est un palindrome
#par exemple:
#[1,7,5,6,6,5,7,1] [8,2,8] [5,5,5,5] [0]
# sont des palindromes
#mais
# [1,2,8,4] [1,2,3,1] n'en sont pas
def palindrome(liste):
    #on parcourt la moitié de la liste
    for i in range(len(liste)//2):
        if liste[i]!=liste[-i-1]:
            return False
    return True

print(palindrome([5,5,5,5]))
print(palindrome([8,2,8]))
print(palindrome([0]))
print(palindrome([1,2,8,4]))
        
def palindrome2(liste):
    return liste==liste[::-1]

print(palindrome2([5,5,5,5]))
print(palindrome2([8,2,8]))
print(palindrome2([0]))
print(palindrome2([1,2,8,4]))

#fonction qui prend en paramètre une liste
#et qui renvoie:
#le minimum ET l'indice du minimum
#interdit d'utiliser min, max et sort

#jeu du "plus haut", "plus bas"
# l'ordi tire un nombre au hasard
#entre 0 et 100
#il demande au joueur un nombre et lui dit
# "plus haut" ou "plus bas"
#jusqu'à ce que le joueur tombe sur le nombre
#score : nombre de coups



