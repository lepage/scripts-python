# -*- coding: utf-8 -*-
class Rectangle:
    def __init__(self,longueur,largeur):
        self.longueur=longueur
        self.largeur=largeur
        
    def __del__(self):
        print("destructeur de la classe Rectangle")
        
    def aire(self):
        return self.longueur*self.largeur
    
    def perimetre(self):
        return 2*(self.longueur+self.largeur)
    
    def get_longueur(self):
        return self.longueur
    
    def get_largeur(self):
        return self.largeur
    
    def set_longueur(self,longueur):
        self.longueur=longueur
    
    def set_largeur(self,largeur):
        self.largeur=largeur
        
class Carre(Rectangle):
    def __init__(self,cote):
        #Rectangle.__init__(self,cote,cote)
        super().__init__(cote,cote)
        
    def set_longueur(self,longueur):
        self.longueur=longueur
        self.largeur=longueur
        
    def set_largeur(self,largeur):
        self.longueur=largeur
        self.largeur=largeur
    
class Cercle:
    pi=3.14
    
    def __init__(self,rayon):
        self.rayon=rayon
        self.diametre=2*self.rayon
        
    def perimetre(self):
        return self.diametre*Cercle.pi
    
    def set_rayon(self,rayon):
        self.rayon=rayon
        self.diametre=2*self.rayon
        
    def rapport_perimetre_sur_diametre():
        return Cercle.pi
    

if 3>2:
    r=Rectangle(1,2)
    
rect=Rectangle(5,3)
print(type(rect))
print(rect.aire())
print(rect.perimetre())

del rect

rect2=Rectangle(8,3)
print(rect2.aire())
print(rect2.perimetre())

c=Cercle(3)
print(c.perimetre())
print(Cercle.rapport_perimetre_sur_diametre())
print(Cercle.pi)

moncarre=Carre(8)
print(moncarre.aire())
moncarre.set_longueur(5)
print(moncarre.get_longueur())
print(moncarre.get_largeur())
