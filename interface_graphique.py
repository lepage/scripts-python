import tkinter as tk

fenetre=tk.Tk()

monlabel=tk.Label(fenetre,text="Nombre de clics : 0")
monlabel.grid(row=0,column=0)

label2=tk.Label(fenetre,text="deuxieme label")
label2.grid(row=0,column=1)

nb_clics=0
def incremente_clics():
    global nb_clics
    nb_clics+=1
    monlabel.config(text="Nombre de clics : "+str(nb_clics))

monbouton=tk.Button(fenetre,text="clique moi",command=incremente_clics)
monbouton.grid(row=1,column=0)

#deuxième bouton : "réinitialiser", qui a pour effet de
#remettre le compteur de clics à zéro

def reinitialise():
    global nb_clics
    nb_clics=0
    monlabel.config(text="Nombre de clics : 0")

bouton2=tk.Button(fenetre,text="reinitialiser",command=reinitialise)
bouton2.grid(row=1,column=1)

largeur=300
hauteur=300
moncanevas=tk.Canvas(fenetre,width=largeur,height=hauteur,background="white")
moncanevas.grid(row=2,column=0,columnspan=2)

#drapeau de l'Ecosse (à peu près)
#si le canevas a une taille de 400x300 et est bleu
# ligne=moncanevas.create_line(0,0,400,300,fill="white",width=50)
# ligne2=moncanevas.create_line(400,0,0,300,fill="white",width=50)

#drapeau du Brésil (à peu près)
#si le canevas a une taille de 400x300 et est vert
# moncanevas.create_polygon(50,150,200,50,350,150,200,250,fill="yellow")
# moncanevas.create_oval(150,100,250,200,fill="blue")
# moncanevas.create_text(210,270,text="ORDEM E PROGRESSO")

#grille de morpion 3x3
moncanevas.create_line(0,hauteur//3,largeur,hauteur//3)
moncanevas.create_line(0,2*hauteur//3,largeur,2*hauteur//3)
moncanevas.create_line(largeur//3,0,largeur//3,hauteur)
moncanevas.create_line(2*largeur//3,0,2*largeur//3,hauteur)

fenetre.mainloop()
