#on crée une liste aléatoire
import random

liste=[random.randint(-100,100)
        for i in range(20)]
print(liste)

#on affiche le plus grand élément de la liste

#maximum va contenir à tout moment le plus
#grand élément rencontré jusqu'ici.
maximum=liste[0]
for e in liste:
    #si je trouve un élément
    #plus grand que le max actuel
    if e > maximum:
        #il devient mon nouveau max
        maximum=e
print(maximum)

