#Écrire 10 nombres aléatoires dans un fichier (1 par ligne)
#ouvrir ce fichier en lecture et calculer la somme
import random as rd

with open("nombres.txt","w") as fichier:
    for i in range(10):
        fichier.write(str(rd.randint(1,100))+"\n")
        
with open("nombres.txt","r") as fichier:
    lignes=fichier.readlines()
    
somme=0
for l in lignes:
    somme+=int(l)
print(somme)

