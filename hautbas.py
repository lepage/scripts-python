#jeu du "plus haut", "plus bas"
# l'ordi tire un nombre au hasard
#entre 0 et 100
#il demande au joueur un nombre et lui dit
# "plus haut" ou "plus bas"
#jusqu'à ce que le joueur tombe
#sur le nombre
#score : nombre de coups

import random as rnd

nombre=rnd.randint(0,100)
compteur=0
essai=-1
while essai!=nombre:
    essai=int(input("Entre un nombre entre 0 et 100 "))
    if essai<nombre:
        print("Plus haut !")
    elif essai>nombre:
        print("Plus bas !")
    compteur+=1
print("Bravo !")
print("Tu as trouvé en",compteur,"coups.")


