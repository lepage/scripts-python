contacts={"Thibaut":"06000000","cle2":"084684086","Toto":"044866828"}

#print(contacts["Thibaut"])

contacts["Thibaut"]="061111111"

#print(contacts["Thibaut"])

contacts["Nicolas"]="0158456589"

#print(contacts)

for i in contacts:
    print(i,contacts[i])
print("")
    
for i in contacts.items():
    print(i)
print("")
    
for cle,valeur in contacts.items():
    print("cle:",cle,", valeur:",valeur)
print("")

#sauvegarder ce dictionnaire dans un fichier repertoire.txt
#dans lequel vous mettrez un contact par ligne
#le nom et le numéro seront séparés par un point-virgule :
#Thibaut;061111111
#cle2;084684086
#Toto;044866828
#Nicolas;0158456589

with open("repertoire.txt","w") as fichier:
    for cle,valeur in contacts.items():
        fichier.write(cle+";"+valeur+"\n")
        
def sauvegarde_dictionnaire(dictionnaire,nomfichier,separateur=" "):
    with open(nomfichier,"w") as fichier:
        for cle,valeur in dictionnaire.items():
            fichier.write(cle+separateur+valeur+"\n")
            
sauvegarde_dictionnaire(contacts,"repertoire2.txt")
            
            