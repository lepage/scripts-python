import random

def de():
    return random.randint(1,6)

def points(de1,de2,de3):
    if de1==de2==de3:
        return 0
        #variante 1: return 1
        #variante 2: return -1
    elif de1==de2:
        return 2*de3
        #variante: return 3*de3
    elif de2==de3:
        return 2*de1
        #variante: return 3*de1
    elif de1==de3:
        return 2*de2
        #variante: return 3*de2
    else:
        return max(de1,de2,de3)
    
def tour():
    print("Les dés roulent...")
    de1=de()
    de2=de()
    de3=de()
    print(de1,de2,de3)
    
    choix=int(input("Quel dé veux-tu relancer ? "))
    if choix>0 and choix<4:
        print("Les dés roulent...")
    if choix==1:
        de1=de()
    elif choix==2:
        de2=de()
    elif choix==3:
        de3=de()
    print(de1,de2,de3)
    
    score=points(de1,de2,de3)
    if score>0:
        print("Tu gagnes",score,"points")
    print("")
    return score

def partie():
    print("Nouvelle partie !")
    #le bloc try/except n'était pas demandé
    #il permet de ne pas produire d'erreur
    #si le fichier n'existe pas encore
    try:
        with open("scores.txt","r") as fichier:
            record=max([int(ligne) for ligne in fichier.readlines()])
            print("Le record est de",record,"points")
    except FileNotFoundError:
        print("Pas de ficher scores.txt trouvé.")
        
    score=0
    for i in range(1,4):
    #variante: for i in range(1,5):
        print("tour",i)
        score=score+tour()
        
    print("Partie terminée !")
    print("Tu as",score,"points")
    print("")
    with open("scores.txt","a") as fichier:
        fichier.write(str(score)+"\n")
    
continuer=True
while continuer:
    partie()
    reponse=input("Veux-tu rejouer ? (O/N) ")
    continuer = reponse == "o" or reponse == "O"
