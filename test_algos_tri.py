import tri,time
import random as rd

liste1=[rd.random()
       for i in range(10000)]

#copie profonde (deep copy)
#de la liste 1
#pour pouvoir modifier les éléments
#d'une liste sans modifier ceux
#de l'autre
liste2=[e for e in liste1]
#ne pas confondre avec une
#shallow copy:
#liste2=liste1
#dans ce cas on copie seulement
#la référence
#print(liste1[:3])
#print(liste2[:3])
#liste1[0]=0
#print(liste1[:3])
#print(liste2[:3])

t1=time.time()
tri.tri_selection(liste1)
t2=time.time()
print("tri selection:",t2-t1)

t1=time.time()
liste2.sort()
t2=time.time()
print("sort():",t2-t1)
