# -*- coding: utf-8 -*-
import tkinter as tk

fenetre=tk.Tk()

largeur_case=100
largeur=3*largeur_case
marge=10
moncanevas=tk.Canvas(fenetre,width=largeur,height=largeur,background="white")
moncanevas.grid(row=0,column=0)
moncanevas.create_line(0,largeur_case,largeur,largeur_case)
moncanevas.create_line(0,2*largeur_case,largeur,2*largeur_case)
moncanevas.create_line(largeur_case,0,largeur_case,largeur)
moncanevas.create_line(2*largeur_case,0,2*largeur_case,largeur)
label=tk.Label(fenetre,text="Au tour du joueur croix")
label.grid(row=1,column=0)

#on représente l'état de la partie à un instant donné par une liste
liste_cases=["vide" for i in range(9)]
#print(liste_cases)
tour=0

#les cases sont numérotées de 0 à 8 dans le sens de la lecture

# def case(x,y):
#     if x<largeur_case:
#         if y<largeur_case:
#             return 0
#         elif y<2*largeur_case:
#             return 3
#         else:
#             return 6
#     elif x<2*largeur_case:
#         if y<largeur_case:
#             return 1
#         elif y<2*largeur_case:
#             return 4
#         else:
#             return 7
#     else:
#         if y<largeur_case:
#             return 2
#         elif y<2*largeur_case:
#             return 5
#         else:
#             return 8

#magie noire pour trouver la case en une ligne
def case(x,y):
    return 3*(y//largeur_case)+x//largeur_case

def dessiner_croix(case):
    x=(case%3)*largeur_case
    y=(case//3)*largeur_case
    moncanevas.create_line(x+marge,y+marge,x+largeur_case-marge,y+largeur_case-marge,
                           fill="blue",width=3)
    moncanevas.create_line(x+marge,y+largeur_case-marge,x+largeur_case-marge,y+marge,
                           fill="blue",width=3)

def dessiner_rond(case):
    x=(case%3)*largeur_case
    y=(case//3)*largeur_case
    moncanevas.create_oval(x+marge,y+marge,x+largeur_case-marge,y+largeur_case-marge,
                           outline="red",width=3)
    
def test_victoire(case):
    ligne=case//3
    colonne=case%3
    #alignement horizontal
    if liste_cases[3*ligne]==liste_cases[3*ligne+1]==liste_cases[3*ligne+2]:
        return True
    #alignement vertical
    if liste_cases[colonne]==liste_cases[colonne+3]==liste_cases[colonne+6]:
        return True
    #alignement diagonal 1
    if ligne==colonne and liste_cases[0]==liste_cases[4]==liste_cases[8]:
        return True
    #alignement diagonal 2
    if ligne==2-colonne and liste_cases[2]==liste_cases[4]==liste_cases[6]:
        return True
    return False
    
def on_click(event):
    global tour
    x=event.x
    y=event.y
    numero_case=case(x,y)
    if(liste_cases[numero_case]=="vide"):
        if tour%2==0:
            dessiner_croix(case(x,y))
            liste_cases[numero_case]="croix"
            if test_victoire(numero_case):
                label.config(text="Le joueur croix a gagné !")
                moncanevas.unbind("<Button-1>")
            else:
                label.config(text="Au tour du joueur rond")
        else:
            dessiner_rond(case(x,y))
            liste_cases[numero_case]="rond"
            if test_victoire(numero_case):
                label.config(text="Le joueur rond a gagné !")
                moncanevas.unbind("<Button-1>")
            else:
                label.config(text="Au tour du joueur croix")
        tour+=1
    
#on associe le clic gauche de la souris à la fonction on_click
moncanevas.bind("<Button-1>",on_click)

fenetre.mainloop()